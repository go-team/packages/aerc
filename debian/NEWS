aerc (0.16.0-1) unstable; urgency=medium

  Aerc builtin filters path (/usr/libexec/aerc/filters) is now prepended to
  the default system PATH to avoid conflicts with other installed binaries
  which have the same name as aerc builtin filters (e.g. /usr/bin/colorize).

  Aerc now has a default style for most UI elements. The default styleset is
  now empty. Existing stylesets will only override the default attributes if
  they are set explicitly. To reset the default style and preserve existing
  stylesets appearance, these two lines must be inserted at the beginning:

    *.default=true
    *.normal=true

 -- Nilesh Patra <nilesh@debian.org>  Sun, 10 Dec 2023 10:00:59 +0530

aerc (0.15.2-1) unstable; urgency=medium

  * Aerc's built-in filters have been moved from /usr/share/aerc/filters to
    /usr/libexec/aerc/filters.  The default exec PATH in aerc's context has
    been modified to include all variations of the libexec subdirs.  If your
    configuration is using absolute paths, you must change it to either point
    to new paths or to use filter names only.  See aerc-config(5) for more
    details.
  * [ui].index-format setting has been replaced by index-columns.  See
    aerc-config(5) for more details.
  * [statusline].render-format has been replaced by status-columns.  See
    aerc-config(5) for more details.
  * [ui:subject...] contextual sections have been replaced by dynamic styleset
    objects.  See aerc-stylesets(7) for more details.
  * [triggers] setting has been replaced by [hooks].  See aerc-config(5) for
    more details.
  * smtp-starttls setting in accounts.conf has been removed.  All smtp://
    transports now assume STARTTLS and will fail if the server does not
    support it.  To disable STARTTLS, use smtp+insecure://.  See aerc-smtp(5)
    for more details.

 -- Robin Jarry <robin@jarry.cc>  Mon, 19 Jun 2023 13:48:59 +0000
